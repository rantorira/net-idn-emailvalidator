#!perl

use 5.18.0;
use strict;
use warnings FATAL => 'all';

use Test::More tests => 1;

#-----------------------------------------------------------------------------

subtest 'Net::IDN::EmailValidator can be loaded' => sub {
    plan tests => 1;

    require_ok( 'Net::IDN::EmailValidator' );
    note(
        "Testing Net::IDN::EmailValidator $Net::IDN::EmailValidator::VERSION,"
      . "Perl $^V, $^X"
    );
};

