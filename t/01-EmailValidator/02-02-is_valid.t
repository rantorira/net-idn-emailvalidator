#!perl

use 5.18.0;
use strict;

use Test::Spec;

use Net::IDN::EmailValidator;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'Net::IDN::EmailValidator';

#=============================================================================
############################### Unit tests ###################################

describe "Method 'is_valid'" => sub {

    #=========================================================================
    # Tests config

    my $class_instance;
    my $domain_validator_stub;

    my $fixtures = {
        any_email           => 'any@email.test',
        valid_email         => 'valid@email.test',
        empty_stub          => '',
        any_string          => '_123',
        any_int             => 123,
        true                => 1,
    };

    # Instance constructor, Dependencies STUBS and MOCKS etc.
    before each => sub {

        # External dependency
        $module_name->stubs( 'validated_hash' => sub {
                my ( $params ) = @{ $_[0] };
                return %$params;
            },
        );

        # External dependency
        $domain_validator_stub = stub();
        $domain_validator_stub->stubs( 'is_valid' );
        $domain_validator_stub->stubs( 'get_length' );
        $domain_validator_stub->stubs( 'get_validation_error' );

        # Constructor
        $class_instance = $module_name->new(
            domain_validator => $domain_validator_stub
        );

        # Internal dependency
        $class_instance->stubs( '_validate' => $fixtures->{ empty_stub } );
    };

    #=========================================================================
    # Test cases

    describe "when done," => sub {

        it "should return '1' if validation is successful" => sub {

            $class_instance->stubs( '_validate' => $fixtures->{ true } );

            # Act
            my $res = $class_instance->is_valid( {
                    email => $fixtures->{ any_email }
                }
            );

            is( $res, 1 );
        };


        it "should return undef if validation is failed" => sub {

            $class_instance->stubs( '_validate' => undef );

            # Act
            my $res = $class_instance->is_valid( {
                    email => $fixtures->{ any_email }
                }
            );

            is( $res, undef );
        };
    };

    #-------------------------------------------------------------------------

    describe "when attribute is given," => sub {

        it "should validate it through 'validated_hash' method" => sub {

            $module_name->expects( 'validated_hash' )->once;

            # Act
            $class_instance->is_valid( { email => $fixtures->{ any_email } } ) ;

            ok 1;
        };


        it "should set it into 'email' attribute " => sub {

            # Act
            # if given one
            my $new_class_instance = $module_name->new(
                domain_validator    => $domain_validator_stub,
                email               => $fixtures->{ valid_email },
            );

            # Need stub
            $new_class_instance->stubs( '_validate' => $fixtures->{ empty_stub } );

            # Act
            # and then given two
            $new_class_instance->is_valid( { email => $fixtures->{ any_email } } );

            # Assert
            # should be two
            is( $new_class_instance->get_email, $fixtures->{ any_email } );
        };
    };

    #-------------------------------------------------------------------------

    describe "at the beginning of each execution," => sub {

        it "should clear persistent attribute '_local_part'" => sub {

            # Assume that attribute has already been set
            # at the previous execution.
            $class_instance->_set_local_part( $fixtures->{ any_string } );

            # Act
            $class_instance->is_valid( { email => $fixtures->{ any_email } } );

            ok not $class_instance->get_local_part;
        };


        it "should clear persistent attribute '_domain_part'" => sub {

            # Assume that attribute has already been set
            # at the previous execution.
            $class_instance->_set_domain_part( $fixtures->{ any_string } );

            # Act
            $class_instance->is_valid( { email => $fixtures->{ any_email } } );

            ok not $class_instance->get_domain_part;
        };


        it "should clear persistent attribute '_local_part_length'" => sub {

            # Assume that attribute has already been set
            # at the previous execution.
            $class_instance->_set_local_part_length( $fixtures->{ any_int } );

            # Act
            $class_instance->is_valid( { email => $fixtures->{ any_email } } );

            is( $class_instance->_get_local_part_length, 0 );
        };


        it "should clear persistent attribute '_validation_error'" => sub {

            # Assume that attribute has already been set
            # at the previous execution.
            $class_instance->_set_validation_error( $fixtures->{ any_string } );

            # Act
            $class_instance->is_valid( { email => $fixtures->{ any_email } } );

            ok not $class_instance->has_validation_error;
        };
    };
};

#=============================================================================
############################### Run tests  ###################################

runtests;

