#!perl

use 5.18.0;
use strict;

use Test::Spec;

use Net::IDN::EmailValidator;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'Net::IDN::EmailValidator';

#=============================================================================
############################### Unit tests ###################################

describe "Method '_invalid'" => sub {

    #=========================================================================
    # Tests config

    my $class_instance;
    my $domain_validator_stub;

    my $fixtures = {
        any_text => '_text_',
    };

    # Instance constructor, Dependencies STUBS and MOCKS etc.
    before each => sub {

        # External dependency
        $domain_validator_stub = stub();
        $domain_validator_stub->stubs( 'is_valid' );
        $domain_validator_stub->stubs( 'get_length' );
        $domain_validator_stub->stubs( 'get_validation_error' );

        # Constructor
        $class_instance = $module_name->new( 
            domain_validator => $domain_validator_stub
        );
    };

    #=========================================================================
    # Test cases

    describe "when done" => sub {

        it "should return undef" => sub {

            is( $class_instance->_invalid(), undef );
        };
    };

    #-------------------------------------------------------------------------

    describe "when executing," => sub {

        it "should set attribute '_validation_error'" => sub {

            # Act
            $class_instance->_invalid();

            ok $class_instance->has_validation_error;
        };
    };

    #-------------------------------------------------------------------------

    describe "when attribute is given" => sub {

        it "should interpolate it into '_validation_error' attribute" => sub {

            # Act
            $class_instance->_invalid( { message => $fixtures->{ any_text } } ) ;

            like( 
                $class_instance->get_validation_error, 
                qr/\Q$fixtures->{ any_text }\E/ 
            );
        };
    };
};

#=============================================================================
############################### Run tests  ###################################

runtests;

