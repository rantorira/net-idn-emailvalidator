#!perl

use 5.18.0;
use strict;

use Test::Spec;

use Net::IDN::EmailValidator;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'Net::IDN::EmailValidator';

#=============================================================================
############################### Unit tests ###################################

describe "Method '_validate'" => sub {

    #=========================================================================
    # Tests config

    my $class_instance;
    my $domain_validator_stub;

    my $fixtures = {
        any_email           => 'any@email.test',
        valid_email         => 'valid@email.test',
        empty_stub          => '',
        local_part_less     => '@email.test',
        domain_part_less    => 'email@',
        true                => 1,
    };

    ( $fixtures->{ valid_local_part } ) =
        $fixtures->{ valid_email } =~ /\A(.*)@.*\Z/;

    ( $fixtures->{ valid_domain_part } ) =
        $fixtures->{ valid_email } =~ /\A.*@(.*)\Z/;

    # Instance constructor, Dependencies STUBS and MOCKS etc.
    before each => sub {

        # External dependency
        $domain_validator_stub = stub();
        $domain_validator_stub->stubs( 'is_valid' );
        $domain_validator_stub->stubs( 'get_length' );
        $domain_validator_stub->stubs( 'get_validation_error' );

        # Constructor
        $class_instance = $module_name->new(
            domain_validator => $domain_validator_stub
        );

        # Internal dependency
        $class_instance->stubs( 
            '_is_local_part_valid' => $fixtures->{ empty_stub } 
        );
    };

    #=========================================================================
    # Test cases

    describe "when validate," => sub {

        before each => sub {

            # External dependency
            $class_instance->expects( 'domain_validator' )->never;

            # Internal dependency
            $class_instance->expects( '_is_local_part_valid' )->never;
            $class_instance->expects( '_invalid' )->returns( undef )->once;
        };


        it "should return undef if nothing to check" => sub {

            is( $class_instance->_validate(), undef );
        };


        it "should return undef if subj has no local part" => sub {

            $class_instance->_set_email( $fixtures->{ local_part_less } );

            # Act & Assert
            is( $class_instance->_validate(), undef );
        };


        it "should return undef if subj has no domain part" => sub {

            $class_instance->_set_email( $fixtures->{ domain_part_less } );

            # Act & Assert
            is( $class_instance->_validate(), undef );
        };
    };

    #-------------------------------------------------------------------------

    describe "when validate domain part," => sub {

        it "should return undef if domain part is invalid" => sub {

            # External dependency
            my $domain_validator_mock = mock();
            $domain_validator_mock->stubs(
                'get_validation_error' => $fixtures->{ empty_stub }
            );

            $domain_validator_mock->expects( 'is_valid' )
                ->returns( undef )
                ->once;

            $class_instance->stubs(
                'domain_validator' => $domain_validator_mock
            );

            # Internal dependency
            $class_instance->expects( '_is_local_part_valid' )->never;
            $class_instance->expects( '_invalid' )->returns( undef )->once;

            $class_instance->_set_email( $fixtures->{ any_email } );

            # Act & Assert
            is( $class_instance->_validate(), undef );
        };
    };

    #-------------------------------------------------------------------------

    describe "when validate local part," => sub {

        it "should return undef if local part is invalid" => sub {

            # External dependency
            my $domain_validator_mock = mock();
            $domain_validator_mock->expects( 'is_valid' )
                ->returns( $fixtures->{ true } )
                ->once;

            $class_instance->stubs(
                'domain_validator' => $domain_validator_mock
            );

            # Internal dependency
            $class_instance->expects( '_is_local_part_valid' )
                ->returns( undef )
                ->once;

            $class_instance->_set_email( $fixtures->{ any_email } );

            # Act & Assert
            is( $class_instance->_validate(), undef );
        };
    };

    #-------------------------------------------------------------------------

    describe "when local and domain parts are valid," => sub {

        describe "if their sum length is longer than 253 characters, " => sub {

            it "should return undef" => sub {

                # External dependency
                my $domain_validator_mock = mock();
                $domain_validator_mock->expects( 'is_valid' )
                    ->returns( $fixtures->{ true } )
                    ->once;

                # domain part length
                $domain_validator_mock->expects( 'get_length' )
                    ->returns( 250 )
                    ->once;

                $class_instance->expects( 'domain_validator' )
                    ->returns( $domain_validator_mock )
                    ->exactly( 2 );

                # Internal dependency
                $class_instance->expects( '_is_local_part_valid' )
                    ->returns( $fixtures->{ true } )
                    ->once;

                # local part length
                $class_instance->expects( '_get_local_part_length' )
                    ->returns( 4 )
                    ->once;

                $class_instance->expects( '_invalid' )->returns( undef )->once;

                $class_instance->_set_email( $fixtures->{ any_email } );

                # Assume that both local and domain parts does not comprises '@' sign.
                # But it is natively considered in EmailValidator.
                # Therefor maximum valid length sould be 253
                #
                # Act & Assert
                is( $class_instance->_validate(), undef );
            };
        };
    };

    #-------------------------------------------------------------------------

    describe "if validation is successful," => sub {

        before each => sub {

            # External dependency
            my $domain_validator_mock = mock();
            $domain_validator_mock->expects( 'is_valid' )
                ->returns( $fixtures->{ true } )
                ->once;

            $domain_validator_mock->expects( 'get_length' )
                ->returns( length $fixtures->{ valid_domain_part } )
                ->once;

            $class_instance->expects( 'domain_validator' )
                ->returns( $domain_validator_mock )
                ->exactly( 2 );

            # Internal dependency
            $class_instance->expects( '_is_local_part_valid' )
                ->returns( $fixtures->{ true } )
                ->once;

            $class_instance->expects( '_get_local_part_length' )
                ->returns( length $fixtures->{ valid_local_part } )
                ->once;

            $class_instance->_set_email( $fixtures->{ valid_email } );
        };


        it "should return '1'" => sub {

            is( $class_instance->_validate(), 1 );
        };


        it "should set attribute '_local_part'" => sub {

            # Act
            $class_instance->_validate();

            is( $class_instance->get_local_part, $fixtures->{ valid_local_part } );
        };


        it "should set attribute '_domain_part'" => sub {

            # Act
            $class_instance->_validate();

            is( $class_instance->get_domain_part, $fixtures->{ valid_domain_part } );
        };
    };
};

#=============================================================================
############################### Run tests  ###################################

runtests;

