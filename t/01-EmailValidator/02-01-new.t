#!perl

use 5.18.0;
use strict;

use Test::Exception;
use Test::MockObject;
use Test::More;

#-----------------------------------------------------------------------------

use Net::IDN::EmailValidator;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'Net::IDN::EmailValidator';

#=============================================================================
############################## Unit tests ####################################

subtest "new() should return correct instance" => sub {
    plan tests => 1;

    {
        my $domain_validator_mock = Test::MockObject->new();
        $domain_validator_mock->mock( 'is_valid', sub { return } );
        $domain_validator_mock->mock( 'get_length', sub { return } );
        $domain_validator_mock->mock( 'get_validation_error', sub { return } );

        # Act
        my $class_instance = $module_name->new(
            domain_validator => $domain_validator_mock
        );

        isa_ok( $class_instance, $module_name );
    }
};

subtest "new() can raise critical exeptions" => sub {
    plan tests => 4;

    {
        Test::Exception::throws_ok(
            # Act
            sub {
                my $class_instance = $module_name->new();
            },

            # Assert
            qr/\(domain_validator\) is required/,
            "Rises exeption if required attribute 'domain_validator' is missed"
        )
    }


    {
        my $incorrect_domain_validator_mock = Test::MockObject->new();
        $incorrect_domain_validator_mock->mock( 'is_valid', sub { return } );

        Test::Exception::throws_ok(
            # Act
            sub {
                my $class_instance = $module_name->new(
                    domain_validator => $incorrect_domain_validator_mock
                );
            },

            # Assert
            qr/object should provides 'get_length'/,
            "Rises exeption if domain validator object have no 'get_length' method"
        );
    }


    {
        my $incorrect_domain_validator_mock = Test::MockObject->new();
        $incorrect_domain_validator_mock->mock( 'get_length', sub { return } );

        Test::Exception::throws_ok(
            # Act
            sub {
                my $class_instance = $module_name->new(
                    domain_validator => $incorrect_domain_validator_mock
                );
            },

            # Assert
            qr/object should provides 'is_valid'/,
            "Rises exeption if domain validator object have no 'is_valid' method"
        );
    }

    {
        my $incorrect_domain_validator_mock = Test::MockObject->new();
        $incorrect_domain_validator_mock->mock( 'get_length', sub { return } );
        $incorrect_domain_validator_mock->mock( 'is_valid', sub { return } );

        Test::Exception::throws_ok(
            # Act
            sub {
                my $class_instance = $module_name->new(
                    domain_validator => $incorrect_domain_validator_mock
                );
            },

            # Assert
            qr/object should provides 'get_validation_error'/,
            "Rises exeption if domain validator object have no 'get_validation_error' method"
        );
    }

};

#=============================================================================
############################### Run tests  ###################################

done_testing();

