#!perl

#****************************************************************************#
#************************* Functional testing *******************************#
#****************************************************************************#

use 5.18.0;
use strict;

use Test::Exception;
use Test::MockObject;
use Test::More;

use Net::IDN::EmailValidator::DomainValidator;
use Net::IDN::EmailValidator;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'Net::IDN::EmailValidator';

binmode( STDOUT, ':utf8' );

#=============================================================================
############################# Object tests ###################################

# Tests config

my $fixtures = {
    valid_email    => 'valid@email.test',
    invalid_email  => 'invalid"@email.test',
};

( $fixtures->{ valid_local_part } ) =
    $fixtures->{ valid_email } =~ /\A(.*)@.*\Z/;

( $fixtures->{ valid_domain_part } ) =
    $fixtures->{ valid_email } =~ /\A.*@(.*)\Z/;


sub class_factory {
    return $module_name->new(
        domain_validator => Net::IDN::EmailValidator::DomainValidator->new()
    );
}

# Test cases
subtest "Method 'is_valid' should work" => sub {
    plan tests => 7;

    {
        my $class_instance = class_factory();

        # Act
        my $res = $class_instance->is_valid();

        is( $res, undef, "Should return undef if nothing to validate" );
    }

    subtest "Should correctly validate a email address if it is valid" => sub {
        plan tests => 2;

        {
            my $class_instance = class_factory();
            my $valid_email = '"(·)(·)"@юникодовые-титьки-валидны.рф';

            # Act
            my $res = $class_instance->is_valid( { email => $valid_email } );

            is( $res, 1, "'$valid_email' is invalid" );
        }


        {
            my $class_instance = class_factory();
            my $valid_email = '{.}{.}@ascii.цыцкі.таксама.валідныя.by';

            # Act
            my $res = $class_instance->is_valid( { email => $valid_email } );

            is( $res, 1, "'$valid_email' is invalid" );
        }

    };

    subtest "Should correctly validate a email address if it is invalid" => sub {
        plan tests => 9;

        {
           my $class_instance = class_factory();
           my $invalid_email = '';

           # Act
           my $res = $class_instance->is_valid( { email => $invalid_email } );

           is( $res, undef, "empty string is invalid" );
        }


        {
            my $class_instance = class_factory();
            my $invalid_email = 'invalid';

            # Act
            my $res = $class_instance->is_valid( { email => $invalid_email } );

            is( $res, undef, "'$invalid_email' is invalid" );
        }


        {
            my $class_instance = class_factory();
            my $invalid_email = '@invalid';

            # Act
            my $res = $class_instance->is_valid( { email => $invalid_email } );

            is( $res, undef, "'$invalid_email' is invalid" );
        }


        {
            my $class_instance = class_factory();
            my $invalid_email = 'invalid@';

            # Act
            my $res = $class_instance->is_valid( { email => $invalid_email } );

            is( $res, undef, "'$invalid_email' is invalid" );
        }


        {
            my $class_instance = class_factory();
            my $invalid_email = 'invalid@@@@@@@';

            # Act
            my $res = $class_instance->is_valid( { email => $invalid_email } );

            is( $res, undef, "'$invalid_email' is invalid" );
        }


        {
            my $class_instance = class_factory();
            my $invalid_email = 'invalid@@@email';

            # Act
            my $res = $class_instance->is_valid( { email => $invalid_email } );

            is( $res, undef, "'$invalid_email' is invalid" );
        }


        {
            my $class_instance = class_factory();
            my $invalid_email = 'invalid@local@test.test';

            # Act
            my $res = $class_instance->is_valid( { email => $invalid_email } );

            is( $res, undef, "'$invalid_email' is invalid. local part is invalid" );
        }


        {
            my $class_instance = class_factory();
            my $invalid_email = 'test@_...test.test';

            # Act
            my $res = $class_instance->is_valid( { email => $invalid_email } );

            is( $res, undef, "'$invalid_email' is invalid. domain part is invalid" );
        }


        {
            my $class_instance = class_factory();
            my $invalid_email = 'valid-local-part-length-26@valid_domain_part_length.'
                              . 'with_115_chars_in_this_string_and_140_concatenated.total_is_255'
                              . ( "x" x 140 );

            # Act
            my $res = $class_instance->is_valid( { email => $invalid_email } );

            # Suppose domain and local parts is valid but total length exceeded. the length is 255
            is( $res, undef, "'$invalid_email' is invalid. total length exceeded" );
        }
    };

    subtest "Should take given argument" => sub {
        plan tests => 2;

        subtest "Should expect `string`" => sub {
            plan tests => 4;

            {
                my $class_instance = class_factory();

                Test::Exception::lives_and(
                    sub {
                        # Act
                        $class_instance->is_valid( { email => '' } );

                        # Assert
                        is $class_instance->get_email, '';
                    },
                    "empty string was given"
                );
            }


            {
                my $class_instance = class_factory();

                Test::Exception::lives_and(
                    sub {
                        # Act
                        $class_instance->is_valid( { email => 0 } );

                        # Assert
                        is $class_instance->get_email, 0;
                    },
                    "0 was given, looks like a single-character string"
                );
            }


            {
                my $class_instance = class_factory();

                Test::Exception::lives_and(
                    sub {
                        # Act
                        $class_instance->is_valid( { email => 0123 } );

                        # Assert
                        is $class_instance->get_email, 83;
                    },
                    "octal 83 was given, that looks like string"
                );
            }


            {
                my $class_instance = class_factory();

                Test::Exception::lives_and(
                    sub {
                        # Act
                        $class_instance->is_valid( { email => 0x20 } );

                        # Assert
                        is $class_instance->get_email, 32;
                    },
                    "hex 32 was given, that looks like string"
                );
            }
        };

        subtest "Can raise critical exeptions when take arguments" => sub {
            plan tests => 2;

            {
                my $class_instance = class_factory();

                Test::Exception::throws_ok(
                    # Act
                    sub {
                        $class_instance->is_valid( {
                                test => $fixtures->{ valid_email }
                            }
                        )
                    },

                    # Assert
                    qr/The following parameter was passed in the call/,
                    "Rises exeption if incorrect argument's name is given"
                );
            };


            {
                my $class_instance = class_factory();

                Test::Exception::throws_ok(
                    # Act
                    sub {
                        $class_instance->is_valid( { email => undef } )
                    },

                    # Assert
                    qr/did not pass the \'checking type constraint for Str\'/,
                    "Rises exeption if no string is given"
                );
            };
        };
    };

    subtest "Should take constructor's argument if method's argument doesn't set" => sub {
        plan tests => 1;

        my $class_instance = $module_name->new(
            domain_validator => Net::IDN::EmailValidator::DomainValidator->new(),
            email => $fixtures->{ valid_email },
        );

        # Act
        $class_instance->is_valid();

        is(
            $class_instance->get_email, $fixtures->{ valid_email },
            "Constructor's argument is valid email"
        );
    };

    subtest "Should provide local and domain parts of given email" => sub {
        plan tests => 2;

        {
            my $class_instance = class_factory();

            # Act
            $class_instance->is_valid( { email => $fixtures->{ valid_email } } );

            is(
                $class_instance->get_local_part,
                $fixtures->{ valid_local_part },
                "'get_local_part' provides rigth local part"
            );
        }


        {
            my $class_instance = class_factory();

            # Act
            $class_instance->is_valid( { email => $fixtures->{ valid_email } } );

            is(
                $class_instance->get_domain_part,
                $fixtures->{ valid_domain_part },
                "'get_local_part' provides rigth domain part"
            );
        }
    };

    subtest "Should provide error text if email is invalid" => sub {
        plan tests => 1;

        {
            my $class_instance = class_factory();
            $class_instance->is_valid( { email => $fixtures->{ invalid_email } } );

            like(
                $class_instance->get_validation_error, qr/.+/,
                "'get_validation_error' has error text"
            );
        }
    };

};

#=============================================================================
############################### Run tests  ###################################

done_testing();

