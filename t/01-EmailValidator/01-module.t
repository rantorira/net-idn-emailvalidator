#!perl

use 5.18.0;
use strict;

use Test::Moose qw( has_attribute_ok );
use Test::More;

use Net::IDN::EmailValidator;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'Net::IDN::EmailValidator';

#=============================================================================
############################ Class tests #####################################

#-----------------------------------------------------------------------------
# Class attributes

subtest "class should have attributes" => sub {
    plan tests => 6;

    my @attrs = ( 'email', '_local_part', '_domain_part', '_validation_error',
        '_local_part_length', 'domain_validator' );

    foreach my $attr ( @attrs ) {
        has_attribute_ok(
            $module_name , $attr, "class has attribute '$attr'"
        );
    }
};

#-----------------------------------------------------------------------------
# Class methods

subtest "class should have methods" => sub {
    plan tests => 11;

    can_ok( $module_name, 'is_valid' );
    can_ok( $module_name, '_validate' );
    can_ok( $module_name, '_is_local_part_valid' );
    can_ok( $module_name, '_invalid' );

    can_ok( $module_name, 'IsIDNAAtsign' );

    subtest "class should have inherited methods" => sub {
        plan tests => 1;

        can_ok( $module_name, 'new' );
    };

    subtest "should have accessors for 'email'" => sub {
        plan tests => 2;

        can_ok( $module_name, '_set_email' );
        can_ok( $module_name, 'get_email' );
    };

    subtest "should have accessors for '_local_part'" => sub {
        plan tests => 3;

        can_ok( $module_name, '_set_local_part' );
        can_ok( $module_name, 'get_local_part' );
        can_ok( $module_name, '_clear_local_part' );
    };

    subtest "should have accessors for '_domain_part'" => sub {
        plan tests => 3;

        can_ok( $module_name, '_set_domain_part' );
        can_ok( $module_name, 'get_domain_part' );
        can_ok( $module_name, '_clear_domain_part' );
    };

    subtest "should have accessors for '_local_part_length'" => sub {
        plan tests => 3;

        can_ok( $module_name, '_set_local_part_length' );
        can_ok( $module_name, '_get_local_part_length' );
        can_ok( $module_name, '_clear_local_part_length' );
    };

    subtest "should have accessors for '_validation_error'" => sub {
        plan tests => 4;

        can_ok( $module_name, '_set_validation_error' );
        can_ok( $module_name, 'get_validation_error' );
        can_ok( $module_name, '_clear_validation_error' );
        can_ok( $module_name, 'has_validation_error' );
    };
};

#=============================================================================
################################ Run tests  ##################################

done_testing();

