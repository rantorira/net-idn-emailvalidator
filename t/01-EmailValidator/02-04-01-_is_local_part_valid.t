#!perl

use 5.18.0;
use strict;

use Test::Spec;

use Net::IDN::EmailValidator;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'Net::IDN::EmailValidator';

#=============================================================================
############################### Unit tests ###################################

describe "Method '_is_local_part_valid'" => sub {

    #=========================================================================
    # Tests config

    my $class_instance;
    my $domain_validator_stub;

    my $fixtures = {
        any_local           => 'any_local_test',
        invalid_local       => 'invalid_@_local',
        valid_local         => 'valid_local_test',
        empty_stub          => '',
        longer_local_part   => 'this_label_comprises_more_than_64_allowable_symbols_herein_them_66',
        true                => 1,
    };

    # Instance constructor, Dependencies STUBS and MOCKS etc.
    before each => sub {

        # External dependency
        $domain_validator_stub = stub();
        $domain_validator_stub->stubs( 'is_valid' );
        $domain_validator_stub->stubs( 'get_length' );
        $domain_validator_stub->stubs( 'get_validation_error' );

        # Constructor
        $class_instance = $module_name->new(
            domain_validator => $domain_validator_stub
        );
    };

    #=========================================================================
    # Test cases

    describe "when executing," => sub {

        it "should set attribute '_local_part_length'" => sub {

            # Act
            $class_instance->_is_local_part_valid( {
                    local_part => $fixtures->{ any_local }
                }
            );

            is(
                $class_instance->_get_local_part_length,
                length $fixtures->{ any_local }
            );
        };
    };

    #-------------------------------------------------------------------------

    describe "when done," => sub {

        it "should return 1 if given subj is valid local part" => sub {

            # Act
            my $res = $class_instance->_is_local_part_valid( {
                    local_part => $fixtures->{ valid_local }
                }
            );

            ok $res;
        };


        it "should return undef if given subj is invalid local part" => sub {

           $class_instance->expects( '_invalid' )->returns( undef )->once;

           # Act
           my $res = $class_instance->_is_local_part_valid( {
                    local_part => $fixtures->{ invalid_local }
                }
            );

            ok not $res;
        };
    };

    #-------------------------------------------------------------------------

    describe "when validate," => sub {

        it "should return undef if given subj length is longer than 64 characters" => sub {

            $class_instance->expects( '_invalid' )->returns( undef )->once;

            # Act
            my $res = $class_instance->_is_local_part_valid( {
                    local_part => $fixtures->{ longer_local_part }
                }
            );

            ok not $res;
        };
    };
};

#=============================================================================
############################### Run tests  ###################################

runtests;

