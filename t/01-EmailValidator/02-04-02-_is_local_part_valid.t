#!perl

use 5.18.0;
use strict;

use Test::MockObject;
use Test::More;

use Net::IDN::EmailValidator;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'Net::IDN::EmailValidator';

binmode( STDOUT, ':utf8' );

#=============================================================================
############################## Unit tests ####################################

# Tests config

sub class_factory {

    # External dependency
    my $domain_validator_mock = Test::MockObject->new();
    $domain_validator_mock->mock( 'is_valid', sub { return } );
    $domain_validator_mock->mock( 'get_length', sub { return } );
    $domain_validator_mock->mock( 'get_validation_error', sub { return } );

    return $module_name->new( domain_validator => $domain_validator_mock );
}

# Test cases
subtest "Method '_is_local_part_valid' when executing, " => sub {
    plan tests => 2;

    subtest "Should correctly validate a local part if it is valid" => sub {
        plan tests => 8;

        {
            my $valid_local = 'ɷа1FÇмвй_ʘ_йёqw';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $valid_local } );

            is( $res, 1, "'$valid_local' is valid" );
        }


        {
            my $valid_local = '!#$%&\'*+-/=?^_`.{|}~';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $valid_local } );

            is( $res, 1, "'$valid_local' is valid" );
        }


        {
            my $valid_local = '"Abc@def"';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $valid_local } );

            is( $res, 1, "'$valid_local' is valid" );
        }


        {
            my $valid_local = '"Joe.\\Blow"';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $valid_local } );

            is( $res, 1, "'$valid_local' is valid" );
        }


        {
            my $valid_local = '"Joe.\\\Blow"';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $valid_local } );

            is( $res, 1, "'$valid_local' is valid" );
        }


        {
            my $valid_local = '"Joe.\\\\\Blow"';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $valid_local } );

            is( $res, 1, "'$valid_local' is valid" );
        }


        {
            my $valid_local = '"\\\"';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $valid_local } );

            is( $res, 1, "'$valid_local' is valid" );
        }


        {
            my $valid_local = '伊昭傑.εχαμπλε';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $valid_local } );

            is( $res, 1, "'$valid_local' is valid" );
        }

    };

    subtest "can correctly validate a local part if it is invalid" => sub {
        plan tests => 10;

        {
            my $invalid_local = '';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $invalid_local } );

            is( $res, undef, "empty string is invalid" );
        }


        {
            my $invalid_local = '   ';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $invalid_local } );

            is( $res, undef, "'$invalid_local' is invalid" );
        }


        {
            my $invalid_local = 'local@part';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $invalid_local } );

            is( $res, undef, "'$invalid_local' is invalid" );
        }


        {
            my $invalid_local = '..test';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $invalid_local } );

            is( $res, undef, "'$invalid_local' is invalid" );
        }


        {
            my $invalid_local = 'test"local\"test';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $invalid_local } );

            is( $res, undef, "'$invalid_local' is invalid" );
        }


        {
            my $invalid_local = 'test"local"test';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $invalid_local } );

            is( $res, undef, "'$invalid_local' is invalid" );
        }


        {
            my $invalid_local = 'test "local".test';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $invalid_local } );

            is( $res, undef, "'$invalid_local' is invalid" );
        }


        {
            my $invalid_local = 'ɷа1FÇмвй_©_йёqw';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $invalid_local } );

            is( $res, undef, "'$invalid_local' is invalid. '©' is no letter" );
        }

        {
            my $invalid_local = '"\\"';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $invalid_local } );

            is( $res, undef, "'$invalid_local' is invalid." );
        }

        {
            my $invalid_local = '"""';
            my $class_instance = class_factory();

            # Act
            my $res = $class_instance->_is_local_part_valid( { local_part => $invalid_local } );

            is( $res, undef, "'$invalid_local' is invalid." );
        }

    };
};

#=============================================================================
############################### Run tests  ###################################

done_testing();

