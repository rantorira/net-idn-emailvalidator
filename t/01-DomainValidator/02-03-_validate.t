#!perl

use 5.18.0;
use strict;

use Test::Spec;

use Net::IDN::EmailValidator::DomainValidator;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'Net::IDN::EmailValidator::DomainValidator';

#=============================================================================
############################### Unit tests ###################################

describe "Method '_validate'" => sub {

    #=========================================================================
    # Tests config

    my $class_instance;
    my $fixtures = {
        valid_domain        => 'valid.test.domain',
        converted_domain    => 'converted.test.domain',
        any_domain          => 'any.test.domain',
        empty_stub          => '',

        # Kind of unicode dot(full stop) 0xFF0E
        leading_dot_domain => '．invalid_domain',

        # Kind of unicode dot(full stop) 0x3002 and 0xFF61
        ending_dots_domain => 'invalid_domain。｡',
    };

    # Instance constructor, Dependencies STUBS and MOCKS etc.
    before each => sub {

        # External dependency
        $module_name->stubs( 'uts46_to_ascii' => $fixtures->{ empty_stub } );

        # Concrtuctor
        $class_instance = $module_name->new();
    };

    #=========================================================================
    # Test cases

    describe "when validate before convertation," => sub {

        before each => sub {

            # External dependency
            $module_name->expects( 'uts46_to_ascii' )->never;

            # Internal dependency
            $class_instance->expects( '_invalid' )
                ->returns( undef )
                ->once;
        };


        it "should return undef if nothing to check" => sub {

            is( $class_instance->_validate(), undef );
        };


        it "should return undef if subj has leading dots" => sub {

            $class_instance->_set_domain( $fixtures->{ leading_dot_domain } );

            # Act & Assert
            is( $class_instance->_validate(), undef );
        };


        it "should return undef if subj has more than one dot at the end" => sub {

            $class_instance->_set_domain( $fixtures->{ ending_dots_domain } );

            # Act & Assert
            is( $class_instance->_validate(), undef );
        };
    };

    #-------------------------------------------------------------------------

    describe "when validate by convertation," => sub {

        it "should return undef if convertation is failed" => sub {

            # External dependency
            $module_name->expects( 'uts46_to_ascii' )
                ->returns( sub { die } )
                ->once;

            # Internal dependency
            $class_instance->expects( '_invalid' )
                ->returns( undef )
                ->once;

            $class_instance->_set_domain( $fixtures->{ any_domain } );

            # Act & Assert
            is( $class_instance->_validate(), undef );
        };


        describe "if convertation is successful," => sub {

            before each => sub {

                $class_instance->_set_domain( $fixtures->{ valid_domain } );

                # External method
                $module_name->expects( 'uts46_to_ascii' )
                    ->returns( $fixtures->{ converted_domain } )
                    ->once;
            };


            it "should return '1'" => sub {

                is( $class_instance->_validate(), 1 );
            };


            it "should set attribute '_length'" => sub {

                # Act
                $class_instance->_validate();

                ok $class_instance->get_length;
            };


            it "attribute '_length' should contain subj length " => sub {

                # Act
                $class_instance->_validate();

                is(
                    $class_instance->get_length,
                    length( $fixtures->{ converted_domain } )
                );
            };


            it "should set attribute '_depth'" => sub {

                # Act
                $class_instance->_validate();

                ok $class_instance->get_depth;
            };


            it "attribute '_depth' should contain subj depth" => sub {

                # Act
                $class_instance->_validate();

                is(
                    $class_instance->get_depth,
                    scalar split /\./, $fixtures->{ converted_domain }
                );
            };
        };
    };
};

#=============================================================================
############################### Run tests  ###################################

runtests;

