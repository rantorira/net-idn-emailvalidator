#!perl

use 5.18.0;
use strict;

use Test::Spec;

use Net::IDN::EmailValidator::DomainValidator;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'Net::IDN::EmailValidator::DomainValidator';

#=============================================================================
############################### Unit tests ###################################

describe "Method 'is_valid'" => sub {

    #=========================================================================
    # Tests config

    my $class_instance;
    my $fixtures = {
        any_domain          => 'any.test.domain',
        valid_domain        => 'valid.test.domain',
        empty_stub          => '',
        any_int             => 123,
        any_string          => '_123',
        true                => 1,
    };

    # Instance constructor, Dependencies STUBS and MOCKS etc.
    before each => sub {

        # External dependency
        $module_name->stubs( 'validated_hash' => sub {
                my ( $params ) = @{ $_[0] };
                return %$params;
            },
        );

        # Concrtuctor
        $class_instance = $module_name->new();

        # Internal dependency
        $class_instance->stubs( '_validate' => $fixtures->{ empty_stub } );
    };

    #=========================================================================
    # Test cases

    describe "when done," => sub {

        it "should return '1' if validation is successful" => sub {

            $class_instance->stubs( '_validate' => $fixtures->{ true } );

            # Act
            my $res = $class_instance->is_valid( {
                    domain => $fixtures->{ any_domain }
                }
            );

            is( $res, 1 );
        };


        it "should return undef if validation is failed" => sub {

            $class_instance->stubs( '_validate' => undef );

            # Act
            my $res = $class_instance->is_valid( {
                    domain => $fixtures->{ any_domain }
                }
            );

            is( $res, undef );
        };
    };

    #-------------------------------------------------------------------------

    describe "when attribute is given," => sub {

        it "should validate it through 'validated_hash' method" => sub {

            $module_name->expects( 'validated_hash' )->once;

            # Act
            $class_instance->is_valid( { domain => $fixtures->{ any_domain } } ) ;

            ok 1;
        };


        it "should set it into 'domain' attribute " => sub {

            # Act
            # if given one
            my $new_class_instance = $module_name->new(
                domain => $fixtures->{ valid_domain }
            );

            # Act
            # and then given two
            $new_class_instance->is_valid( {
                    domain => $fixtures->{ any_domain }
                }
            );

            # Assert
            # should be two
            is( $new_class_instance->get_domain, $fixtures->{ any_domain } );
        };
    };

    #-------------------------------------------------------------------------

    describe "at the beginning of each execution," => sub {

        it "should clear persistent attribute '_length'" => sub {

            # Assume that attribute has already been set
            # at the previous execution.
            $class_instance->_set_length( $fixtures->{ any_int } );

            # Act
            $class_instance->is_valid( { domain => $fixtures->{ any_domain } } );

            is( $class_instance->get_length, 0 );
        };


        it "should clear persistent attribute '_depth'" => sub {

            # Assume that attribute has already been set
            # at the previous execution.
            $class_instance->_set_depth( $fixtures->{ any_int } );

            # Act
            $class_instance->is_valid( { domain => $fixtures->{ any_domain } } );

            is( $class_instance->get_depth, 0 );
        };


        it "should clear persistent attribute '_validation_error'" => sub {

            # Assume that attribute has already been set
            # at the previous execution.
            $class_instance->_set_validation_error( $fixtures->{ any_string } );

            # Act
            $class_instance->is_valid( { domain => $fixtures->{ any_domain } } );


            ok not $class_instance->has_validation_error;
        };
    };
};

#=============================================================================
############################### Run tests  ###################################

runtests;

