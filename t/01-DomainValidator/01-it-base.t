#!perl

#****************************************************************************#
#************************* Functional testing *******************************#
#****************************************************************************#

use 5.18.0;
use strict;

use Test::Exception;
use Test::More;

use Net::IDN::EmailValidator::DomainValidator;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'Net::IDN::EmailValidator::DomainValidator';

binmode( STDOUT, ':utf8' );

#=============================================================================
############################# Object tests ###################################

# Tests config
my $fixtures = {
    valid_domain    => 'valid.test.domain',
    invalid_domain  => '++.##.invalid_domain',
};

# Test cases
subtest "Method 'is_valid' should work" => sub {
    plan tests => 7;

    {
        my $class_instance = $module_name->new();

        # Act
        my $res = $class_instance->is_valid();

        # Assert
        is( $res, undef, "Should return undef if nothing to validate" );
    }

    subtest "Should correctly validate a domain if it is valid" => sub {
        plan tests => 7;

        {
            my $valid_domain = 'test.test.test';
            my $class_instance = $module_name->new();

            # Act
            my $res = $class_instance->is_valid( { domain => $valid_domain } );

            # Assert
            is( $res, 1, "'$valid_domain' is valid" );
        }


        {
            my $valid_domain = 'test';
            my $class_instance = $module_name->new();

            # Act
            my $res = $class_instance->is_valid( { domain => $valid_domain } );

            # Assert
            is( $res, 1, "'$valid_domain' one level lable can be valid" );
        }


        {
            my $valid_domain = 'test.';
            my $class_instance = $module_name->new();

            # Act
            my $res = $class_instance->is_valid( { domain => $valid_domain } );

            # Assert
            is( $res, 1, "$valid_domain' one dot at the end is valid" );
        }


        {
            my $valid_domain = '郵件.商務';
            my $class_instance = $module_name->new();

            # Act
            my $res = $class_instance->is_valid( { domain => $valid_domain } );

            # Assert
            is( $res, 1, "$valid_domain' is valid" );
        }


        {
            my $valid_domain = 'मोहन.ईन्फो';
            my $class_instance = $module_name->new();

            # Act
            my $res = $class_instance->is_valid( { domain => $valid_domain } );

            # Assert
            is( $res, 1, "$valid_domain' is valid" );
        }


        {
            my $valid_domain = '__tes_t__';
            my $class_instance = $module_name->new( UseSTD3ASCIIRules => 0 );

            # Act
            my $res = $class_instance->is_valid( { domain => $valid_domain } );

            # Assert
            is( $res, 1, "$valid_domain underscore is valid while 'UseSTD3ASCIIRules' is false" );
        }


        {
            my $valid_domain =
                'a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.'
              . 'a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.'
              . 'a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.'
              . 'a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.a.z.'
              . 'a.z.a.z.a.z.a.z.a.z.a.z.a.z.r';

            my $class_instance = $module_name->new();

            # Act
            my $res = $class_instance->is_valid( { domain => $valid_domain } );

            # Assert
            is( $res, 1, "127 lables can be valid" );
        }
    };

    subtest "Should correctly validate a domain if it is invalid" => sub {
        plan tests => 10;

        {
            my $invalid_domain = '';
            my $class_instance = $module_name->new();

            # Act
            my $res = $class_instance->is_valid( { domain => $invalid_domain } );

            # Assert
            is( $res, undef, "empty string is invalid" );
        }


        {
            my $invalid_domain = '.test';
            my $class_instance = $module_name->new();

            # Act
            my $res = $class_instance->is_valid( { domain => $invalid_domain } );

            # Assert
            is( $res, undef, "'$invalid_domain' first dot is invalid" );
        }


        {
            my $invalid_domain = 'test..';
            my $class_instance = $module_name->new();

            # Act
            my $res = $class_instance->is_valid( { domain => $invalid_domain } );

            # Assert
            is( $res, undef, "'$invalid_domain' double and more dots at the end is invalid" );
        }


        {
            my $invalid_domain = '-test';
            my $class_instance = $module_name->new();

            # Act
            my $res = $class_instance->is_valid( { domain => $invalid_domain } );

            # Assert
            is( $res, undef, "'$invalid_domain' first minus-hyphen is invalid" );
        }


        {
            my $invalid_domain = 'test-';
            my $class_instance = $module_name->new();

            # Act
            my $res = $class_instance->is_valid( { domain => $invalid_domain } );

            # Assert
            is( $res, undef, "'$invalid_domain' last minus-hyphen is invalid" );
        }


        {
            my $invalid_domain = 'te--st';
            my $class_instance = $module_name->new();

            # Act
            my $res = $class_instance->is_valid( { domain => $invalid_domain } );

            # Assert
            is( $res, undef, "'$invalid_domain' 3rd adn 4th minus-hyphen is invalid" );
        }


        {
            my $invalid_domain = 'test@test.test';
            my $class_instance = $module_name->new();

            # Act
            my $res = $class_instance->is_valid( { domain => $invalid_domain } );

            # Assert
            is( $res, undef, "'$invalid_domain' '\@' is invalid while 'UseSTD3ASCIIRules' is true" );
        }


        {
            my $invalid_domain = 'tes_t';
            my $class_instance = $module_name->new();

            # Act
            my $res = $class_instance->is_valid( { domain => $invalid_domain } );

            # Assert
            is( $res, undef, "'$invalid_domain' underscore is invalid while 'UseSTD3ASCIIRules' is true" );
        }


        {
            my $invalid_domain = '㏇εχαμπλε.ψομ';
            my $class_instance = $module_name->new();

            # Act
            my $res = $class_instance->is_valid( { domain => $invalid_domain } );

            # Assert
            is( $res, undef, "'$invalid_domain' is invalid. '㏇' is disallowed" );
        }


        {
            my $invalid_domain = 'l1.this_lable_comprises_64_symbols_'
                               . 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.l3.com';
            my $class_instance = $module_name->new();

            # Act
            my $res = $class_instance->is_valid( { domain => $invalid_domain } );

            # Assert
            is( $res, undef, "each of lables can't be longer than 63 symbols" );
        }
    };

    subtest "Should take given argument" => sub {
        plan tests => 2;

        subtest "Should expect `string`" => sub {
            plan tests => 4;

            {
                my $class_instance = $module_name->new();

                Test::Exception::lives_and(
                    sub {
                        # Act
                        $class_instance->is_valid( { domain => '' } );

                        # Assert
                        is $class_instance->get_domain, '';
                    },
                    "empty string was given"
                );
            }


            {
                my $class_instance = $module_name->new();

                Test::Exception::lives_and(
                    sub {
                        # Act
                        $class_instance->is_valid( { domain => 0 } );

                        # Assert
                        is $class_instance->get_domain, 0;
                    },
                    "0 was given, looks like a single-character string"
                );
            }


            {
                my $class_instance = $module_name->new();

                Test::Exception::lives_and(
                    sub {
                        # Act
                        $class_instance->is_valid( { domain => 0123 } );

                        # Assert
                        is $class_instance->get_domain, 83;
                    },
                    "octal 83 was given, that looks like string"
                );
            }


            {
                my $class_instance = $module_name->new();

                Test::Exception::lives_and(
                    sub {
                        # Act
                        $class_instance->is_valid( { domain => 0x20 } );

                        # Assert
                        is $class_instance->get_domain, 32;
                    },
                    "hex 32 was given, that looks like string"
                );
            }
        };

        subtest "Can raise critical exeptions when take arguments" => sub {
            plan tests => 2;

            {
                my $class_instance = $module_name->new();

                Test::Exception::throws_ok(

                    # Act
                    sub {
                        $class_instance->is_valid( { test => $fixtures->{ valid_domain } } )
                    },

                    # Assert
                    qr/The following parameter was passed in the call/,
                    "Rises exeption if incorrect argument's name is given"
                );
            };


            {
                my $class_instance = $module_name->new();

                Test::Exception::throws_ok(

                    # Act
                    sub {
                        $class_instance->is_valid( { domain => undef } )
                    },

                    # Assert
                    qr/did not pass the \'checking type constraint for Str\'/,
                    "Rises exeption if no string is given"
                );
            };
        };
    };

    subtest "Should take constructor's argument if method's argument doesn't set" => sub {
        plan tests => 1;

        my $class_instance = $module_name->new( domain => $fixtures->{ valid_domain } );

        # Act
        $class_instance->is_valid();

        # Assert
        is(
            $class_instance->get_domain, $fixtures->{ valid_domain },
            "Constructor's argument is valid domain"
        );
    };

    subtest "Should provide domain length and depth if it is valid" => sub {
        plan tests => 2;

        {
            my $valid_domain = 'l1.l2.l3.l4.l5.l6.l7.l8.l9.10';
            my $class_instance = $module_name->new();

            # Act
            $class_instance->is_valid( { domain => $valid_domain } );

            # Assert
            is( 
                $class_instance->get_depth, 10, 
                "'get_depth' provides rigth count of lables" 
            );
        }

        {
            my $valid_domain = 'this-lable-comprises-31-symbols';
            my $class_instance = $module_name->new();

            # Act
            $class_instance->is_valid( { domain => $valid_domain } );

            # Assert
            is( 
                $class_instance->get_length, 31, 
                "'get_length' provides right length of given domain" 
            );
        }
    };

    subtest "Should provide error text if domain is invalid" => sub {
        plan tests => 1;

        {
            my $class_instance = $module_name->new();

            # Act
            $class_instance->is_valid( { domain => $fixtures->{ invalid_domain } } );

            # Assert
            like( 
                $class_instance->get_validation_error, qr/.+/, 
                "'get_validation_error' has error text" 
            );
        }
    };

};

#=============================================================================
############################### Run tests  ###################################

done_testing();

