#!perl

use 5.18.0;
use strict;

use Test::Moose qw( has_attribute_ok );
use Test::More;

use Net::IDN::EmailValidator::DomainValidator;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'Net::IDN::EmailValidator::DomainValidator';

#=============================================================================
############################ Class tests #####################################

#-----------------------------------------------------------------------------
# Class attributes

subtest "class should have attributes" => sub {
    plan tests => 5;

    my @attrs = ( 'domain', '_length', '_depth',
        '_validation_error', 'use_std3_ascii_rules' );

    foreach my $attr ( @attrs ) {
        has_attribute_ok(
            $module_name , $attr, "class has attribute '$attr'"
        );
    }
};

#-----------------------------------------------------------------------------
# Class methods

subtest "class should have methods" => sub {
    plan tests => 10;

    can_ok( $module_name, 'is_valid' );
    can_ok( $module_name, '_validate' );
    can_ok( $module_name, '_invalid' );

    can_ok( $module_name, 'IsIDNADot' );

    subtest "class should have inherited methods" => sub {
        plan tests => 1;

        can_ok( $module_name, 'new' );
    };

    subtest "should have accessors for 'domain'" => sub {
        plan tests => 2;

        can_ok( $module_name, '_set_domain' );
        can_ok( $module_name, 'get_domain' );
    };

    subtest "should have accessors for '_length'" => sub {
        plan tests => 2;

        can_ok( $module_name, '_set_length' );
        can_ok( $module_name, 'get_length' );
    };

    subtest "should have accessors for '_depth'" => sub {
        plan tests => 3;

        can_ok( $module_name, '_set_depth' );
        can_ok( $module_name, 'get_depth' );
        can_ok( $module_name, '_clear_depth' );
    };

    subtest "should have accessors for '_validation_error'" => sub {
        plan tests => 4;

        can_ok( $module_name, '_set_validation_error' );
        can_ok( $module_name, 'get_validation_error' );
        can_ok( $module_name, '_clear_validation_error' );
        can_ok( $module_name, 'has_validation_error' );
    };

    subtest "should have accessors for 'use_std3_ascii_rules'" => sub {
        plan tests => 1;

        can_ok( $module_name, 'is_set_use_std3_ascii_rules' );
    };
};

#=============================================================================
############################### Run tests  ###################################

done_testing();

