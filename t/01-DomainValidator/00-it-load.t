#!perl

use 5.18.0;
use strict;
use warnings FATAL => 'all';

use Test::More tests => 1;

#-----------------------------------------------------------------------------

subtest 'Net::IDN::EmailValidator::DomainValidator can be loaded' => sub {
    plan tests => 1;

    require_ok( 'Net::IDN::EmailValidator::DomainValidator' );
    note(
        "Testing Net::IDN::EmailValidator::DomainValidator"
      . "$Net::IDN::EmailValidator::DomainValidator::VERSION,"
      . " Perl $^V, $^X"
    );
};

