#!perl

use 5.18.0;
use strict;

use Test::Spec;

use Net::IDN::EmailValidator::DomainValidator;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'Net::IDN::EmailValidator::DomainValidator';

#=============================================================================
############################### Unit tests ###################################

describe "Method '_invalid'" => sub {

    #=========================================================================
    # Tests config

    my $class_instance;
    my $fixtures = {
        any_text => '_text_',
    };

    before each => sub {
        $class_instance = $module_name->new();
    };

    #=========================================================================
    # Test cases

    describe "when done" => sub {

        it "should return undef" => sub {

            is( $class_instance->_invalid(), undef );
        };
    };

    #-------------------------------------------------------------------------

    describe "when executing," => sub {

        it "should set attribute '_validation_error'" => sub {

            # Act
            $class_instance->_invalid();

            ok $class_instance->has_validation_error;
        };
    };

    #-------------------------------------------------------------------------

    describe "when attribute is given" => sub {

        it "should interpolate it into '_validation_error' attribute" => sub {

            # Act
            $class_instance->_invalid( { message => $fixtures->{ any_text } } ) ;

            like(
                $class_instance->get_validation_error,
                qr/\Q$fixtures->{ any_text }\E/
            );
        };
    };
};

#=============================================================================
############################### Run tests  ###################################

runtests;

