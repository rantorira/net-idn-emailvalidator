#
# Documentation is at the __END__
#

package Net::IDN::EmailValidator::DomainValidator {

    use Modern::Perl '2014';
    use Moose;
    use MooseX::Params::Validate qw( validated_hash );
    use Net::IDN::UTS46 qw( uts46_to_ascii );
    use Try::Tiny;

    #-------------------------------------------------------------------------

    # Except is necessary for opportunity to mock these methods
    use namespace::autoclean -except => [ 'validated_hash', 'uts46_to_ascii' ];
    use utf8;

    #=========================================================================
    ## Constants

    our $VERSION = '0.01';

    # International 'dots' in unicode
    # it used in regexp as unicode sequence
    sub IsIDNADot { "002E\n3002\nFF0E\nFF61" }

    #=========================================================================
    ## Attributes

    # Should contain a domain name for validation
    has 'domain' => (
        is              => 'ro',
        isa             => 'Str',
        documentation   => 'Source string.',
        writer          => '_set_domain',
        reader          => 'get_domain',
    );

    #-------------------------------------------------------------------------
    ## Private attributes

    # Will contain a length of domain after validation
    has '_length' => (
        is              => 'ro',
        isa             => 'Int',
        documentation   => 'FQDN length after the IDNA conversion.',
        writer          => '_set_length',
        reader          => 'get_length',
        default         => 0,
        init_arg        => undef,
        lazy            => 1,
        clearer         => '_clear_length',
    );

    # Will contain a count of lables after validation
    has '_depth' => (
        is              => 'ro',
        isa             => 'Int',
        documentation   => 'Depth of sub domains.',
        writer          => '_set_depth',
        reader          => 'get_depth',
        default         => 0,
        init_arg        => undef,
        lazy            => 1,
        clearer         => '_clear_depth',
    );

    # Will contain a validation error after validation if domain is invalide
    has '_validation_error' => (
        is              => 'ro',
        isa             => 'Str',
        documentation   => "Validation error.",
        writer          => '_set_validation_error',
        reader          => 'get_validation_error',
        init_arg        => undef,
        clearer         => '_clear_validation_error',
        predicate       => 'has_validation_error',
    );

    #-------------------------------------------------------------------------
    ## Flags

    # Specify IDNA2003 Rules
    has 'use_std3_ascii_rules' => (
        is              => 'ro',
        isa             => 'Bool',
        documentation   => 'Flag. Specify if IDNA2003 is avalible.'
          . ' For more information look'
          . ' http://unicode.org/reports/tr46/#IDNA2003-Section',
        reader          => 'is_set_use_std3_ascii_rules',
        default         => 1,
        init_arg        => 'UseSTD3ASCIIRules',
    );

    #=========================================================================
    ## Methods

    sub is_valid {
        my $self = shift;
        my ( $params_ref ) = {
            validated_hash(
                \@_,
                domain => { isa => 'Str', optional => 1 }
            )
        };

        # These attributes always should be clean,
        # to avoid persistence of state for each execution.
        # exemple :
        #
        # # set "Domain is empty" into '_validation_error'
        # $domain_validator->is_valid( domain => '' );
        #
        # # does not set anything into '_validation_error'
        # $domain_validator->is_valid( domain => 'valid.com' );
        #
        # # BAG !!! '_validation_error' has "Domain is empty"
        # say $domain_validator->get_validation_error;

        $self->_clear_length;
        $self->_clear_depth;
        $self->_clear_validation_error;

        if ( defined $params_ref->{ domain } ) {
            $self->_set_domain( $params_ref->{ domain } );
        }

        return $self->_validate();
    }

    #=========================================================================
    ## PRIVATE methods

    sub _validate {
        my $self = shift;

        # Invalid if nothing to check
        return $self->_invalid( {
                message => "Domain is empty"
            }
        ) unless $self->get_domain;


        # Invalid if has leading dots
        return $self->_invalid( {
                message => "Has leading dots"
            }
        ) if $self->get_domain =~ /\A\p{IsIDNADot}+/;


        # Invalid if has more than one dot at the end
        return $self->_invalid( {
                message => "Has more than one dot at the end"
            }
        ) if $self->get_domain =~ /\p{IsIDNADot}{2,}\Z/;


        # Validation by convertation
        # for more useful information look to :
        # http://tools.ietf.org/html/rfc3490
        # http://tools.ietf.org/html/rfc5890#section-2.3.2.1
        # http://www.unicode.org/reports/tr46/
        # http://search.cpan.org/~cfaerber/Net-IDN-Encode-2.201/lib/Net/IDN/Standards.pod#DIFFERENT_STANDARDS

        my $converted_domain = try {
            uts46_to_ascii(
                $self->get_domain,
                UseSTD3ASCIIRules => $self->is_set_use_std3_ascii_rules
            );
        }
        catch {
            chomp;

            # Invalid because convertation does not pass
            $self->_invalid( { message => $_ } );
        };

        return unless $converted_domain;

        $self->_set_length( length $converted_domain );
        $self->_set_depth( scalar split /\p{IsIDNADot}/, $converted_domain );

        return 1;
    }

    sub _invalid {
        my ( $self, $params_ref ) = @_;

        my $mgs_text = "Invalid domain.";

        $mgs_text .= ' [ ' . $self->get_domain . ' ]'   if $self->get_domain;
        $mgs_text .= ' - ' . $params_ref->{ message }   if $params_ref->{ message };

        $self->_set_validation_error( $mgs_text );

        return;
    }

    __PACKAGE__->meta->make_immutable();

};

#-----------------------------------------------------------------------------

1;

__END__

=pod

=head1 NAME

Net::IDN::EmailValidator::DomainValidator - Simple domain validator.

=head1 VERSION

Version 0.01

=head1 SYNOPSIS

    use Net::IDN::EmailValidator::DomainValidator;

    my $obj = Net::IDN::EmailValidator::DomainValidator->new( domain => 'domain' );
    say "Okay!" if $obj->is_valid();

    # or

    my $obj = Net::IDN::EmailValidator::DomainValidator->new();
    say "Okay!" if $obj->is_valid( { domain => 'domain' } );

=head1 DESCRIPTION

Provides simple interface for validating given domain name following IDN
agreement. Based on L<Net::IDN::Encode>.

=head1 ATTRIBUTES

=head2 UseSTD3ASCIIRules

Compliance with IDNA2003.

    my $obj = Net::IDN::EmailValidator::DomainValidator->new();
    say $obj->is_set_use_std3_ascii_rules; # prints: 1

    # reject compliance with IDNA2003
    my $obj = Net::IDN::EmailValidator::DomainValidator->new( UseSTD3ASCIIRules => 0 );
    say $obj->is_set_use_std3_ascii_rules; # prints: 0

See L</SEE ALSO>.

=head1 METHODS

=head2 is_valid()

    $obj->is_valid( { domain => 'domain' } );

This method does validation. In fact it uses C<uts46_to_ascii> from 
L<Net::IDN::Encode> for convertation of given string to punycode with·
Unicode Technical Standard #46 (Unicode IDNA Compatibility Processing).

If given string satisfies UTS #46 and can be converted in punycode, 
then it is valid IDN, but it does not means that exists.

See L</SEE ALSO>.

=head2 get_domain()

Returns current domain name.

=head2 get_length()

If domain name is valid, it returns length of punycode converted domain.

=head2 get_depth()

If domain name is valid, it returns count of included labels.

=head2 get_validation_error()

Returns text message with description of cause, if validation is failed.

=head2 has_validation_error()

True, if validation is failed.

=head1 SEE ALSO

Validation:

http://tools.ietf.org/html/rfc3490
http://tools.ietf.org/html/rfc5890#section-2.3.2.1
http://www.unicode.org/reports/tr46/
http://search.cpan.org/~cfaerber/Net-IDN-Encode-2.201/lib/Net/IDN/Encode.pm

Standarts:

http://search.cpan.org/~cfaerber/Net-IDN-Encode-2.201/lib/Net/IDN/Standards.pod#DIFFERENT_STANDARDS

=head1 AUTHOR

Art Li, <job.lit.art at gmail.com>


=cut
