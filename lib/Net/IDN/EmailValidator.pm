#
# Documentation is at the __END__
#

package Net::IDN::EmailValidator {

    use Modern::Perl '2014';
    use Carp qw( confess );
    use Moose;
    use MooseX::Params::Validate qw( validated_hash );

    #-------------------------------------------------------------------------

    # Except is necessary for opportunity to mock these methods
    use namespace::autoclean -except => [ 'validated_hash' ];
    use utf8;

    #=========================================================================
    ## Constants

    our $VERSION = '0.01';

    # International 'at' in unicode
    # it used in regexp as unicode sequence
    sub IsIDNAAtsign { "0040\nFE6B\nFF20" }

    #=========================================================================
    ## Attributes

    # Should contain an email address for validation
    has 'email' => (
        is              => 'ro',
        isa             => 'Str',
        documentation   => 'source string',
        writer          => '_set_email',
        reader          => 'get_email',
    );

    #-------------------------------------------------------------------------
    ## Private attributes

    # Will contain a local part of email address after validation
    has '_local_part' => (
        is              => 'ro',
        isa             => 'Str',
        documentation   => 'Local part of email.',
        writer          => '_set_local_part',
        reader          => 'get_local_part',
        init_arg        => undef,
        clearer         => '_clear_local_part',
    );

    # Will contain a domain part of email address after validation
    has '_domain_part' => (
        is              => 'ro',
        isa             => 'Str',
        documentation   => "Domain's part of email.",
        writer          => '_set_domain_part',
        reader          => 'get_domain_part',
        init_arg        => undef,
        clearer         => '_clear_domain_part',
    );

    # Will contain a length of local part after validation
    has '_local_part_length' => (
        is              => 'ro',
        isa             => 'Int',
        documentation   => "Lacal part's length.",
        writer          => '_set_local_part_length',
        reader          => '_get_local_part_length',
        default         => 0,
        init_arg        => undef,
        lazy            => 1,
        clearer         => '_clear_local_part_length',
    );

    # Will contain a validation error after validation if email is invalide
    has '_validation_error' => (
        is              => 'ro',
        isa             => 'Str',
        documentation   => "Validation error.",
        writer          => '_set_validation_error',
        reader          => 'get_validation_error',
        init_arg        => undef,
        clearer         => '_clear_validation_error',
        predicate       => 'has_validation_error',
    );

    #-------------------------------------------------------------------------
    ## Components

    # Object provides domain validation
    has 'domain_validator' => (
        is              => 'ro',
        isa             => 'Object',
        required        => 1,
        documentation   => "Object that provides three methods."
          . " 'is_valid', 'get_validation_error' and"
          . " 'get_length'. 'is_valid' should take a HashRef as"
          . " { domain => 'string' } and must return '1' if the"
          . " 'string' is valid domain name or 'False' otherwise."
          . " 'get_validation_error' should return validation"
          . " error text, if domain is invalid. "
          . " 'get_length' should return full length of 'string'"
          . " that was given in to 'is_valid' method. ",
    );

    #=========================================================================
    ## Methods

    sub is_valid {
        my $self = shift;
        my ( $params_ref ) = {
            validated_hash(
                \@_,
                email => { isa => 'Str', optional => 1 }
            )
        };

        # These attributes always should be clean,
        # to avoid persistence of state for each execution
        # exemple :
        #
        # # This - set "Email is empty" into '_validation_error'
        # $email_validator->is_valid( email => '' );
        #
        # # This - does not set anything into '_validation_error'
        # $email_validator->is_valid( email => 'valid@email.com' );
        #
        # # BAG !!! '_validation_error' has "Email is empty"
        # say $email_validator->get_validation_error;

        $self->_clear_local_part;
        $self->_clear_domain_part;
        $self->_clear_local_part_length;
        $self->_clear_validation_error;

        if ( defined $params_ref->{ email } ) {
            $self->_set_email( $params_ref->{ email } );
        }

        return $self->_validate();
    }

    #=========================================================================
    ## PRIVATE methods

    sub _validate {
        my $self = shift;

        # Invalid if nothing to check
        return $self->_invalid( {
                message => 'Email is empty'
            }
        ) unless $self->get_email;


        # Since most chars, including "@", is valid in IDNA2008,
        # but email address can contain only one "@", would be to consider
        # that all repeated "@" are local part,
        # except last "@" which splits domain part
        my ( $local_part, $domain_part ) =
          $self->get_email =~ /\A(.*)\p{IsIDNAAtsign}(.*)\Z/x;


        # Invalid if there is no local or domain part
        return $self->_invalid( {
                message => '"Local" or "Domain" part is empty'
            }
        ) unless $local_part && $domain_part;


        # Invalid if domain part is invalid
        return $self->_invalid( {
                message => $self->domain_validator->get_validation_error
            }
        ) unless $self->domain_validator->is_valid( {
                domain => $domain_part
            }
        );


        # Invalid if local part is invalid
        return unless $self->_is_local_part_valid( {
                local_part => $local_part
            }
        );


        # Invalid if full email address length is longer than 254 characters
        # local + @ + domain = ( local + domain ) + @ = 253 + 1
        # http://www.rfc-editor.org/errata_search.php?rfc=3696&eid=1690
        return $self->_invalid( {
                message => 'Total email address length is longer than 254 characters'
            }
        ) if $self->domain_validator->get_length + $self->_get_local_part_length > 253;

        $self->_set_local_part( $local_part );
        $self->_set_domain_part( $domain_part );

        return 1;
    }

    sub _is_local_part_valid {
        my ( $self, $params_ref ) = @_;

        # Characters instead octets
        $self->_set_local_part_length( length $params_ref->{ local_part } );

        # Invalid if longer than 64 characters
        # http://tools.ietf.org/html/rfc5321#section-4.5.3.1.1
        # https://tools.ietf.org/html/rfc2821#section-4.5.3.1
        return $self->_invalid( {
                message => '"Local" part length is longer than 64 characters'
            }
        ) if $self->_get_local_part_length > 64;


        # Slightly modified version of simple rfc822 validation. original look here:
        # http://perldoc.perl.org/5.14.0/perlfaq9.html#How-do-I-check-a-valid-mail-address?
        #
        # Fully like the original
        my $quoted     = qr{"(?:\\[^\r\n]|[^\\"])*"}x;

        # Changed. [a-zA-Z etc.] to [\p{Letter} etc.] because i18n is available
        my $atom       = qr{[\p{Letter}0-9_!#\$\%&'*+/=?\^`{}~|\-]+}x;

        # Changed. because both can be dot separated
        my $dot_atom   = qr{$atom(?:\.$atom|\.$quoted)*}x;
        my $dot_quoted = qr{$quoted(?:\.$atom|\.$quoted)*}x;

        # Like the original scheme
        my $local      = qr{(?:$dot_atom|$dot_quoted)}x;

        # Invalid if no pass the simple rfc822 validation
        # for more useful information look to :
        # https://tools.ietf.org/html/rfc822#section-6
        # https://tools.ietf.org/html/rfc2822#section-3
        # https://tools.ietf.org/html/rfc5335#section-4.3
        # https://tools.ietf.org/html/rfc3696#section-3
        # https://tools.ietf.org/html/rfc5322#section-3.4

        return $self->_invalid( {
                message => '"Local" part is invalid'
            }
        ) unless $params_ref->{ local_part } =~ /^${local}$/x;

        return 1;
    }

    sub _invalid {
        my ( $self, $params_ref ) = @_;

        my $mgs_text = "Invalid email.";

        $mgs_text .= ' [ ' . $self->get_email . ' ]'    if $self->get_email;
        $mgs_text .= ' - ' . $params_ref->{ message }   if $params_ref->{ message };

        $self->_set_validation_error( $mgs_text );

        return;
    }

    #=========================================================================
    ## Constructor methods

    sub BUILD {
        my $self = shift;

        confess "Domain validator object should provides 'is_valid' method"
          unless $self->domain_validator->can( 'is_valid' );

        confess "Domain validator object should provides 'get_length' method"
          unless $self->domain_validator->can( 'get_length' );

        confess "Domain validator object should provides 'get_validation_error' method"
          unless $self->domain_validator->can( 'get_validation_error' );
    }

    __PACKAGE__->meta->make_immutable();

};

#-----------------------------------------------------------------------------

1;

__END__

=pod

=head1 NAME

Net::IDN::EmailValidator - Simple email validator.

=head1 VERSION

Version 0.01

=head1 SYNOPSIS

    use Net::IDN::EmailValidator;
    use Net::IDN::EmailValidator::DomainValidator;

    my $email = Net::IDN::EmailValidator->new(
        domain_validator => Net::IDN::EmailValidator::DomainValidator->new(),
    );

    say "Okay!" if $email->is_valid( email => '伊昭傑@郵件.商務' );

    # or

    use Net::IDN::EmailValidator;
    use MyExample::DomainValidator::Greek;

    my $email = Net::IDN::EmailValidator->new(
        domain_validator => MyExample::DomainValidator::Greek->new(),
        email => 'email@εχαμπλε.ψομ',
    );

    say "Okay!" if $email->is_valid();


=head1 DESCRIPTION

Provides simple interface for validating email address following IDN email
agreement. Allows to validate local and domain parts separately.
See L</domain_validator>

=head1 CONSTRUCTOR

    my $obj = Net::IDN::EmailValidator->new(
       domain_validator => ...
    );

C<new()> expects mandatory arguments C<domain_validator>. It should be
object that provides three methods. C<is_valid>, C<get_validation_error> and
C<get_length>. C<is_valid> should take a HashRef as C<{ domain => 'string' }>
and must return '1' if the 'string' is valid domain name or 'False' otherwise.
C<get_validation_error> should return validation error text,
if domain is invalid. C<get_length> should return full length of 'string' that
was given in to 'is_valid' method.

=head1 METHODS

=head2 is_valid()

    $obj->is_valid( email => 'email@address.net' );

This method does validation. Considered valid, email, which length no longer 
than 254 characters, and local part no longer than 64 characters. For unquoted 
constituent of local part is allowed numerals C<0-9> sumbols 
C<_!#\$\%&'*+/=?\^`{}~|\-]+> and Unicode letters group C<p{Letter}>.

=head2 get_email()

Returns current email address.

=head2 get_local_part()

If email address is valid, it returns local part.

=head2 get_domain_part()

If email address is valid, it returns domain part.

=head2 get_validation_error()

Returns text message with description of cause, if validation is failed.

=head2 has_validation_error()

True, if validation is failed.

=head1 SEE ALSO

Email address length:

http://www.rfc-editor.org/errata_search.php?rfc=3696&eid=1690

Local part length:

https://tools.ietf.org/html/rfc5321#section-4.5.3.1.1
https://tools.ietf.org/html/rfc2821#section-4.5.3.1

Charset or octets:

https://tools.ietf.org/html/rfc3629
https://tools.ietf.org/html/rfc2278

Validation:

https://tools.ietf.org/html/rfc822#section-6
https://tools.ietf.org/html/rfc2822#section-3
https://tools.ietf.org/html/rfc5335#section-4.3
https://tools.ietf.org/html/rfc3696#section-3
https://tools.ietf.org/html/rfc5322#section-3.4

=head1 AUTHOR

Art Li, <job.lit.art at gmail.com>


=cut
